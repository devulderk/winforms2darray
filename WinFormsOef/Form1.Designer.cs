﻿namespace WinFormsOef
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtsum1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSum2 = new System.Windows.Forms.TextBox();
            this.btnSum = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDiv = new System.Windows.Forms.Button();
            this.btnSub = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sum";
            // 
            // txtsum1
            // 
            this.txtsum1.Location = new System.Drawing.Point(16, 29);
            this.txtsum1.Name = "txtsum1";
            this.txtsum1.Size = new System.Drawing.Size(46, 20);
            this.txtsum1.TabIndex = 1;
            this.txtsum1.TextChanged += new System.EventHandler(this.txtSum1_TextChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "+";
            // 
            // txtSum2
            // 
            this.txtSum2.Location = new System.Drawing.Point(88, 29);
            this.txtSum2.Name = "txtSum2";
            this.txtSum2.Size = new System.Drawing.Size(46, 20);
            this.txtSum2.TabIndex = 3;
            this.txtSum2.TextChanged += new System.EventHandler(this.txtSum2_TextChanged);
            // 
            // btnSum
            // 
            this.btnSum.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSum.ForeColor = System.Drawing.Color.HotPink;
            this.btnSum.Location = new System.Drawing.Point(16, 65);
            this.btnSum.Name = "btnSum";
            this.btnSum.Size = new System.Drawing.Size(46, 23);
            this.btnSum.TabIndex = 4;
            this.btnSum.Text = "Sum";
            this.btnSum.UseVisualStyleBackColor = false;
            this.btnSum.Click += new System.EventHandler(this.btnSum_Click);
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(159, 29);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(46, 20);
            this.txtResult.TabIndex = 5;
            this.txtResult.TextChanged += new System.EventHandler(this.txtResult_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(140, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "=";
            // 
            // btnDiv
            // 
            this.btnDiv.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnDiv.ForeColor = System.Drawing.Color.HotPink;
            this.btnDiv.Location = new System.Drawing.Point(88, 65);
            this.btnDiv.Name = "btnDiv";
            this.btnDiv.Size = new System.Drawing.Size(46, 23);
            this.btnDiv.TabIndex = 7;
            this.btnDiv.Text = "Div";
            this.btnDiv.UseVisualStyleBackColor = false;
            // 
            // btnSub
            // 
            this.btnSub.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSub.ForeColor = System.Drawing.Color.HotPink;
            this.btnSub.Location = new System.Drawing.Point(159, 65);
            this.btnSub.Name = "btnSub";
            this.btnSub.Size = new System.Drawing.Size(46, 23);
            this.btnSub.TabIndex = 8;
            this.btnSub.Text = "Sub";
            this.btnSub.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(222, 100);
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.btnDiv);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.btnSum);
            this.Controls.Add(this.txtSum2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtsum1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Calculator$";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtsum1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSum2;
        private System.Windows.Forms.Button btnSum;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDiv;
        private System.Windows.Forms.Button btnSub;
    }
}

